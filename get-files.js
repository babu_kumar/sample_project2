const fs = require('fs');
const got =require('got');


async function getContent(url, fileFrom, saveTo) {
  try {
    const response = await got(url);
	fs.appendFile(`${saveTo}`, response.body, function (err) {
	  if (err) throw err;
	  console.log(`Saved ${fileFrom} in ${saveTo}`);
	});
  } catch (error) {
	console.log(error.response.body);
  }
}


var chargeList = [
  {
    fileFrom: 'first.txt',
    saveTo: 'charge_1.html'
  },
  {
    fileFrom: 'second.txt',
    saveTo: 'charge_2.html'
  },
  {
    fileFrom: 'third.txt',
    saveTo: 'charge_3.html'
  }
];


fs.mkdirSync('Src');
fs.mkdirSync('Src/Charges');


for (const itr of chargeList) {
  let url = `https://gitlab.com/api/v4/projects/28133493/repository/files/mails%2F${itr.fileFrom}/raw?private_token=${ACCESS_TOKEN}&ref=main`;
  let saveTo = `Src/Charges/${itr.saveTo}`;
  getContent(url, itr.fileFrom, saveTo);
}




var payoutList = [
  {
    fileFrom: 'first.txt',
    saveTo: 'payout_1.html'
  },
  {
    fileFrom: 'second.txt',
    saveTo: 'payout_2.html'
  },
  {
    fileFrom: 'third.txt',
    saveTo: 'payout_3.html'
  }
];


fs.mkdirSync('Src/Payouts');

for (const itr of payoutList) {
  let url = `https://gitlab.com/api/v4/projects/28133493/repository/files/mails%2F${itr.fileFrom}/raw?private_token=${ACCESS_TOKEN}&ref=main`;
  let saveTo = `Src/Payouts/${itr.saveTo}`;
  getContent(url, itr.fileFrom, saveTo);
}




fs.mkdirSync('Src/Onboarding');
